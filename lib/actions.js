'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _actions = require('./core/actions');

Object.defineProperty(exports, 'initRedux', {
  enumerable: true,
  get: function get() {
    return _actions.initRedux;
  }
});