'use strict';

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

var _createAction = require('./createAction');

var _createAction2 = _interopRequireDefault(_createAction);

var _createReducer = require('./createReducer');

var _createReducer2 = _interopRequireDefault(_createReducer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var locationReceived = (0, _createAction2.default)('locationReceived', _tcomb2.default.String);
var LocationsType = _tcomb2.default.struct({
  locations: _tcomb2.default.list(_tcomb2.default.String)
});
var emptyState = { locations: [] };

describe('create reducer', function () {

  test('returns a reducer creator', function () {
    var reducer = (0, _createReducer2.default)(function (state, action) {
      return {
        locations: { $push: [action.payload] }
      };
    }).matching(locationReceived);

    var newState = reducer(LocationsType)(emptyState, locationReceived('a location'));

    expect(newState).toEqual({
      locations: ['a location']
    });
  });
});