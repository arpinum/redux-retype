'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (name) {
    var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _tcomb2.default.Nil;
    return function (thunk) {
        type = !_tcomb2.default.isType(type) && _lodash2.default.isObject(type) ? _tcomb2.default.struct(type) : type;

        var actionName = name + 'ActionCreator';

        return _tcomb2.default.func([_tcomb2.default.maybe(type)], _tcomb2.default.Function, actionName).of(function () {
            var payload = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;
            return thunk(payload);
        });
    };
};