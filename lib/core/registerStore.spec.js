'use strict';

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

var _registerStore = require('./registerStore');

var _registerStore2 = _interopRequireDefault(_registerStore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LocationsType = _tcomb2.default.struct({
  locations: _tcomb2.default.list(_tcomb2.default.String)
}, 'Locations');

describe('initialize state', function () {
  test('returns type & initial state', function () {
    var state = (0, _registerStore2.default)(LocationsType, {
      locations: []
    });

    expect(state).toMatchObject({
      meta: LocationsType,
      initialState: {
        locations: []
      }
    });
  });

  test('builds type from a given descriptor', function () {
    var state = (0, _registerStore2.default)({
      locations: _tcomb2.default.list(_tcomb2.default.String)
    }, {
      locations: []
    }, 'Locations');

    expect(state.meta.meta).toMatchObject({
      kind: 'struct',
      name: 'Locations'
    });
  });
});