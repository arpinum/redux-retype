'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

var _types = require('./types');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Domain = function () {
  function Domain(name) {
    _classCallCheck(this, Domain);

    this.name = name;
    this.actions = [];
    this.reducers = [];
  }

  _createClass(Domain, [{
    key: 'getName',
    value: function getName() {
      return this.name;
    }
  }]);

  return Domain;
}();

exports.default = Domain;