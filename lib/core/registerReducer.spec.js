'use strict';

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

var _createAction = require('./createAction');

var _createAction2 = _interopRequireDefault(_createAction);

var _createReducer = require('./createReducer');

var _createReducer2 = _interopRequireDefault(_createReducer);

var _registerStore = require('./registerStore');

var _registerStore2 = _interopRequireDefault(_registerStore);

var _registerReducer = require('./registerReducer');

var _registerReducer2 = _interopRequireDefault(_registerReducer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('register reducer', function () {
  test('returns the initialState during initialization', function () {
    var locationReceived = (0, _createAction2.default)('locationReceived', _tcomb2.default.String);

    var locationReceivedReducer = (0, _createReducer2.default)(function (state, action) {
      return {
        locations: { $push: [action.payload] }
      };
    }).matching(locationReceived);

    var locationStore = (0, _registerStore2.default)({
      locations: _tcomb2.default.list(_tcomb2.default.String)
    }, {
      locations: []
    }, 'Locations');

    var reducer = (0, _registerReducer2.default)([locationReceivedReducer]).reducing(locationStore);
    var emptyState = reducer(undefined, { type: '@@redux/INIT' });

    expect(emptyState).toEqual({
      locations: []
    });
  });

  test('returns the new state', function () {
    var locationReceived = (0, _createAction2.default)('locationReceived', _tcomb2.default.String);

    var locationReceivedReducer = (0, _createReducer2.default)(function (state, action) {
      return {
        locations: { $push: [action.payload] }
      };
    }).matching(locationReceived);

    var locationStore = (0, _registerStore2.default)({
      locations: _tcomb2.default.list(_tcomb2.default.String)
    }, {
      locations: []
    }, 'Locations');

    var reducer = (0, _registerReducer2.default)([locationReceivedReducer]).reducing(locationStore);
    var emptyState = reducer(undefined, { type: '@@redux/INIT' });

    var newState = reducer(emptyState, locationReceived('a new location'));

    expect(newState).toEqual({
      locations: ['a new location']
    });
  });
});