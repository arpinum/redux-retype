'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (declarativeReducer) {
  return {
    matching: function matching(actionCreatorType) {
      return function (stateType) {
        var fsaType = actionCreatorType.instrumentation.codomain;

        return _tcomb2.default.func([stateType, fsaType], stateType, _tcomb2.default.getTypeName(fsaType) + 'Reducer').of(function (state, action) {
          return stateType.update(state, declarativeReducer(state, action));
        });
      };
    }
  };
};