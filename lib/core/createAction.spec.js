'use strict';

var _createAction = require('./createAction');

var _createAction2 = _interopRequireDefault(_createAction);

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe('createAction', function () {

  test('returns a default FSA', function () {
    var action = (0, _createAction2.default)('locationReceived');

    expect(action()).toMatchObject({
      type: 'LOCATION_RECEIVED',
      payload: undefined,
      error: false
    });
  });

  test('returns a default value, if not provided', function () {
    var action = function action() {
      var args = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'a location';
      return (0, _createAction2.default)('locationReceived', _tcomb2.default.String)(args);
    };

    expect(action()).toMatchObject({
      type: 'LOCATION_RECEIVED',
      payload: 'a location',
      error: false
    });
  });

  test('returns a given payload', function () {
    var locationReceived = (0, _createAction2.default)('locationReceived', _tcomb2.default.String);

    var action = locationReceived('a location');

    expect(action).toMatchObject({
      type: 'LOCATION_RECEIVED',
      payload: 'a location',
      error: false
    });
  });

  describe('with error', function () {

    test('returns a default fsa', function () {
      var locationReceived = (0, _createAction2.default)('locationReceived');

      var action = locationReceived(true);

      expect(action).toMatchObject({
        error: true
      });
    });

    test('returns a fsa with given value', function () {
      var locationReceived = (0, _createAction2.default)('locationReceived', _tcomb2.default.String);

      var action = locationReceived('a location', true);

      expect(action).toMatchObject({
        error: true
      });
    });

    test('has a default value and returns a fsa with given value', function () {
      var locationReceived = function locationReceived() {
        var args = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'a location';
        var error = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
        return (0, _createAction2.default)('locationReceived', _tcomb2.default.String)(args, error);
      };

      var action = locationReceived('another location', true);

      expect(action).toMatchObject({
        payload: 'another location',
        error: true
      });
    });
  });
});