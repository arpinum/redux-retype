'use strict';

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

var _effects = require('redux-saga/effects');

var _registerSaga = require('./registerSaga');

var _registerSaga2 = _interopRequireDefault(_registerSaga);

var _createAction = require('./createAction');

var _createAction2 = _interopRequireDefault(_createAction);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dumbAction = (0, _createAction2.default)('dumbAction', _tcomb2.default.String);
var actionToListen = (0, _createAction2.default)('actionToListen', _tcomb2.default.String);

var aSaga = regeneratorRuntime.mark(function aSaga() {
  return regeneratorRuntime.wrap(function aSaga$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return put(dumbAction('TRY'));

        case 2:
        case 'end':
          return _context.stop();
      }
    }
  }, aSaga, this);
});

var registeredSaga = (0, _registerSaga2.default)(aSaga).matchingEvery(actionToListen);

describe('register saga', function () {
  test('returns a saga', function () {
    var next = registeredSaga().next();

    expect(next.value).toEqual((0, _effects.takeEvery)('ACTION_TO_LISTEN', aSaga));
  });
});