'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _types = require('./types');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (name) {
  var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : _tcomb2.default.Nil;

  type = !_tcomb2.default.isType(type) && _lodash2.default.isObject(type) ? _tcomb2.default.struct(type) : type;

  var actionName = name + 'ActionCreator';
  var fsaType = _lodash2.default.snakeCase(name).toUpperCase();
  var actionType = (0, _types.FSA)(type, fsaType);

  return _tcomb2.default.Nil === type ? //FSA without payload, arity is maybe(boolean)
  _tcomb2.default.func([_tcomb2.default.maybe(_tcomb2.default.Boolean)], actionType, actionName).of(function () {
    var error = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
    return {
      type: fsaType,
      payload: undefined,
      error: error
    };
  }) : _tcomb2.default.func([type, _tcomb2.default.maybe(_tcomb2.default.Boolean)], actionType, actionName).of(function (payload) {
    var error = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    return {
      type: fsaType,
      payload: payload,
      error: error
    };
  });
};