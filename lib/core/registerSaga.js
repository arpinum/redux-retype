'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

var _effects = require('redux-saga/effects');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (saga) {
  return {
    matchingEvery: function matchingEvery(actionCreatorType) {
      return regeneratorRuntime.mark(function _callee() {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return (0, _effects.takeEvery)(_tcomb2.default.getTypeName(actionCreatorType.instrumentation.codomain), saga);

              case 2:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      });
    },
    matchingLatest: function matchingLatest(actionCreatorType) {
      return regeneratorRuntime.mark(function _callee2() {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return (0, _effects.takeLatest)(_tcomb2.default.getTypeName(actionCreatorType.instrumentation.codomain), saga);

              case 2:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this);
      });
    }
  };
};