'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (type, initState) {
  var name = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;


  if (!_tcomb2.default.isType(type)) {
    type = _tcomb2.default.struct(type, name);
  }

  if (type.meta.name === undefined) {
    throw new TypeError('Tcomb struct used to build a state must have a name');
  }

  return {
    initialState: type(initState),
    meta: type
  };
};