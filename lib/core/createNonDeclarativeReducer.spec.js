'use strict';

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

var _createAction = require('./createAction');

var _createAction2 = _interopRequireDefault(_createAction);

var _createNonDeclarativeReducer = require('./createNonDeclarativeReducer');

var _createNonDeclarativeReducer2 = _interopRequireDefault(_createNonDeclarativeReducer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var locationReceived = (0, _createAction2.default)('locationReceived', _tcomb2.default.String);
var LocationsType = _tcomb2.default.struct({
  locations: _tcomb2.default.list(_tcomb2.default.String)
});
var emptyState = { locations: [] };

describe('create reducer', function () {

  test('returns a reducer creator', function () {
    var reducer = (0, _createNonDeclarativeReducer2.default)(function (state, action) {
      return {
        locations: [].concat(_toConsumableArray(state.locations), [action.payload])
      };
    }).matching(locationReceived);

    var newState = reducer(LocationsType)(emptyState, locationReceived('a location'));

    expect(newState).toEqual({
      locations: ['a location']
    });
  });
});