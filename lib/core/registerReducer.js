'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (reducers) {
  return {
    reducing: function reducing(initialState) {
      if (!_lodash2.default.isArray(reducers)) throw new TypeError('you must provide an array of reducer to registerReducer');

      //we build a map, so we have constant look up time to find the corresponding reducer
      var reducerResolver = new Map(reducers.map(function (reducer) {
        reducer = reducer(initialState.meta);
        return [_tcomb2.default.getTypeName(reducer.instrumentation.domain[1]), reducer];
      }));

      return function () {
        var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState.initialState;
        var action = arguments[1];

        var reducer = reducerResolver.get(action.type);
        return reducer === undefined ? state : reducer(state, action);
      };
    }
  };
};