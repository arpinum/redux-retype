'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FSA = undefined;

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FSA = exports.FSA = function FSA(payloadType) {
  var name = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'FSA';
  return _tcomb2.default.interface({
    type: _tcomb2.default.String,
    payload: payloadType,
    error: _tcomb2.default.Boolean
  }, name);
};