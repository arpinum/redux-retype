'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.put = exports.take = exports.fork = exports.call = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _effects = require('redux-saga/effects');

Object.defineProperty(exports, 'call', {
  enumerable: true,
  get: function get() {
    return _effects.call;
  }
});
Object.defineProperty(exports, 'fork', {
  enumerable: true,
  get: function get() {
    return _effects.fork;
  }
});
Object.defineProperty(exports, 'take', {
  enumerable: true,
  get: function get() {
    return _effects.take;
  }
});
var put = exports.put = function put(action) {
  return (0, _effects.put)(_extends({}, action));
};