'use strict';

var _tcomb = require('tcomb');

var _tcomb2 = _interopRequireDefault(_tcomb);

var _createThunkAction = require('./createThunkAction');

var _createThunkAction2 = _interopRequireDefault(_createThunkAction);

var _createAction = require('./createAction');

var _createAction2 = _interopRequireDefault(_createAction);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var myAction = (0, _createAction2.default)('myAction', _tcomb2.default.Number);
var getState = function getState() {
    return { value: 1 };
};

describe('create Thunk', function () {

    test('it creates a thunk', function () {

        var dispatch = jest.fn();

        var addNToStoreValue = (0, _createThunkAction2.default)('addNToStoreValue', _tcomb2.default.Number)(function (valueToAdd) {
            return function (dispatch, getState) {
                dispatch(myAction(getState().value + valueToAdd));
            };
        });

        addNToStoreValue(2)(dispatch, getState);

        expect(dispatch).toBeCalledWith({
            type: 'MY_ACTION',
            payload: 3,
            error: false
        });
    });
});