
## Why this library

- Really tiny & simple
- All in one, battery included, stack for react & redux projects: redux-sagas, react-router & many helpful helpers.
- Action, Reducer, Store & Sagas are typed by default: thanks to [tcomb](https://github.com/gcanti/tcomb), we have runtime types.
- Descriptive Reducer: you provide only a description of the mutation you want to perform.

## Actions

The `createAction` function returns a typed FSA compliant action creator:    
`createAction(actionName: string, [payloadType: tcombType, [defaultValue: object]]) => function`.

### Usage

 - To create a simple action without payload : `const myAction = createAction('myAction');`
 - To create a function with a typed payload : `const myAction = createAction('myAction', t.String);`
 - To create a function with a typed payload and a default value : `const myAction = createAction('myAction', t.String, 'default');`
 
`createAction` returns an action creator that check payload type:
 
 ```js
const myAction = createAction('myAction', t.String, 'default');
const fsaWithDefault = myAction(); //fsa: {type: 'MY_ACTION', payload: 'default', error: false, meta: {tcomb metadata}}
const fsaWithValue = myAction('my_value');  //fsa: {type: 'MY_ACTION', payload: 'my_value', error: false, meta: {tcomb metadata}}
const invalidFsa = myAction(5); //will throw a TypeError exception because the given argument doesn't have the proper type
```

As a FSA compliant action creator, the function returned by `createAction` accepts a second boolean parameters to tells if the action is the result of an error:

```js
const myAction = createAction('myAction');
const fsa = myAction();
const fsaMarkedAsErrored = myAction(true); //also work with a payload: myActionWithPayload('my value', true);
```

Warning: even if your action creator has a default value, in order to flag an action as the result of an error you must provide a payload:

```js
const myAction = createAction('myAction', t.String, 'default');
const fsaMarkedAsErrored = myAction(true); //invalid syntax
const fsaMarkedAsErrored = myAction('a string payload', true); //correct syntax
```

## Reducer

The `createReducer` function gives you the ability to build a descriptive reducer and attach it to a specific action:   
`createReducer(descriptiveReducer: (state, action) => newStateDescription).for(actionCreator: type) => function`.

### Usage

The reducer system is called "descriptive" because it only returns a descriptive representation inspired by [immutability-helper](https://github.com/kolodny/immutability-helper) of the state update to perfom:

```js
const myAction = createAction('myAction', t.String);
const myReducer = createReducer((state, action) => ({
    foo: {$push: [action.payload]}
})).for(myAction);
```

Unfortunately this is not sufficient, we need to provide the data-structure on which the update has to be performed among with its initial state.
This is why we have an `initializeState` helper: `initializeState(state: type).with(initialState: object);`

```js
const myState = initializeState({
    foo: t.list(t.String),
    bar: t.Boolean
}).with({
    foo: [],
    bar: false
});
```

Then we can register the reducers that will perform update on this state tree with the `registerReducer` function: 
`registerReducer(reducers: list<descriptiveReducer>).for(initializedState: object)`:

```js
const reducers = registerReducers([myReducer]).for(myState);
```

A more realistic example:

```js
const userType = t.struct({
    email: t.String,
    firstName: t.String,
    lastName: t.String
});

const addUser = createAction('addUser', userType);
const removeUser = createAction('removeUser', t.String);

const userState = initializeState({
    users: t.list(userType)
}).with({
    users: []
});

const addUserReducer = createReducer((state, action) => ({
    users: {$push: [action.payload]}
})).for(addUser);
const removeUserReducer = createReducer((state, action) => {
    const userToRemoveIndex = state.users.findIndex(user => user.email == action.payload);
    return {
      users: {$splice: [userToRemoveIndex, 1]}
    };
}).for(removeUser);

const reducer = registerReducer([addUserReducer, removeUserReducer]).for(userState);

//adding user (after redux connect):
addUser({
    email: 'mikael@mikael.org',
    firstName: 'mikael',
    lastName: 'jerry'
});
```

## Resources to consider:
- Tcomb (type system): https://github.com/gcanti/tcomb
- Immutability Helper (state update descriptive syntax): https://github.com/kolodny/immutability-helper