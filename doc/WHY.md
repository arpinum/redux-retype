#Why this library ?
##A standalone library, not another starter/boilerplate kit
This project is intended to be used as a standalone library, installed through npm (yarn). This enables you the capacity to get update from the retype components during the whole lifecycle of your application.

##Strongly runtime typed
We extensively use the tcomb project to enable runtime type validation, from action creator to reducer. This secures your development by catching during the development phase some nasty bug.

##Avoid a lot of boilerplate code
When developing a redux application, creating action, reducers and managing sides effects can generate a lot of duplications. By providing you a lot of helpful helpers, retype get you the capacity to use shorter way to declare your redux components.

##Simple and natural API
We believe in simplicity, this is why our api is intended to be simple to use in a very natural way. We strongly believe in the "simple made easy" motto from Rich Hickey.

##Descriptive side effects & state management
We use immutable helpers (provided by tcomb) to build descriptive reducers which guarantee a complete immutable system to perform updates on your store. Plus it gives you another capacity to test your reducers by testing the description of a state transition instead of the state transition by itself.

On the other hand, redux saga gives a fully descriptive mechanism to describe side effets operations. Welcome in the heaven of testing complex I/O orchestration.

##Fit in any existing Redux project
Because we only add some enhancement to your redux project you can still continue to use any pre-existing code or library as it without making any change to your codebase