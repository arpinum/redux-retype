import t from 'tcomb';

export default declarativeReducer => ({
  matching: actionCreatorType => stateType => {
    const fsaType = actionCreatorType.instrumentation.codomain;

    return t.func([stateType, fsaType], stateType, `${t.getTypeName(fsaType)}Reducer`).of(
      (state, action) => stateType.update(state, declarativeReducer(state, action))
    );
  }
});