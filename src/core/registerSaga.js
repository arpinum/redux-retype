import t from 'tcomb';
import {takeEvery, takeLatest} from 'redux-saga/effects'

export default saga => ({
  matchingEvery: actionCreatorType => function*() {
    yield takeEvery(t.getTypeName(actionCreatorType.instrumentation.codomain), saga);
  },
  matchingLatest: actionCreatorType => function*() {
    yield takeLatest(t.getTypeName(actionCreatorType.instrumentation.codomain), saga);
  }
});