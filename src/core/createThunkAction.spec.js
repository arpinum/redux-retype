import t from 'tcomb';
import createThunkAction from './createThunkAction';
import createAction from './createAction';

const myAction = createAction('myAction', t.Number);
const getState = () => ({value: 1});

describe('create Thunk', () => {

    test('it creates a thunk', () => {

        const dispatch = jest.fn();

        const addNToStoreValue = createThunkAction('addNToStoreValue', t.Number)(valueToAdd => (dispatch, getState) => {
            dispatch(myAction(getState().value + valueToAdd));
        });

        addNToStoreValue(2)(dispatch, getState);

        expect(dispatch).toBeCalledWith({
            type: 'MY_ACTION',
            payload: 3,
            error: false,
        });
    });

});