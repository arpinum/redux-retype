import _ from 'lodash';
import t from 'tcomb';

export default reducers => ({
  reducing: initialState => {
    if(!_.isArray(reducers)) throw new TypeError('you must provide an array of reducer to registerReducer');


    //we build a map, so we have constant look up time to find the corresponding reducer
    const reducerResolver = new Map(reducers.map(reducer => {
      reducer = reducer(initialState.meta);
      return [
        t.getTypeName(reducer.instrumentation.domain[1]),
        reducer
      ];
    }));

    return (state = initialState.initialState, action) => {
      const reducer = reducerResolver.get(action.type);
      return (reducer === undefined) ? state : reducer(state, action);
    }
  }
});