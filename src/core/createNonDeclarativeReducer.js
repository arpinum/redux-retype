import t from 'tcomb';

export default nonDeclarativeReducer => ({
  matching: actionCreatorType => stateType => {
    const fsaType = actionCreatorType.instrumentation.codomain;
    return t.func([stateType, fsaType], stateType, `${t.getTypeName(fsaType)}NonDeclarativeReducer`).of(nonDeclarativeReducer);
  }
});