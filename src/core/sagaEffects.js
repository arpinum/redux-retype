import {put as putEffect} from 'redux-saga/effects';

export {call, fork, take} from 'redux-saga/effects';
export const put = (action) => putEffect({...action});