import t from 'tcomb';
import _ from 'lodash';
import { FSA } from './types';

export default (name, type = t.Nil) => {
  type = (!t.isType(type) && _.isObject(type)) ? t.struct(type) : type;

  const actionName = `${name}ActionCreator`;
  const fsaType = _.snakeCase(name).toUpperCase();
  const actionType = FSA(type, fsaType);

  return (t.Nil === type) ? ( //FSA without payload, arity is maybe(boolean)
    t.func([t.maybe(t.Boolean)], actionType, actionName).of((error = false) => ({
      type: fsaType,
      payload: undefined,
      error: error
    }))
  ) : (
    t.func([type, t.maybe(t.Boolean)], actionType, actionName).of((payload, error = false) => ({
      type: fsaType,
      payload: payload,
      error: error
    }))
  );
}
