import t from 'tcomb';
import createAction from './createAction';
import createReducer from './createNonDeclarativeReducer';

const locationReceived = createAction('locationReceived', t.String);
const LocationsType = t.struct({
  locations: t.list(t.String)
});
const emptyState = {locations: []};

describe('create reducer', () => {

  test('returns a reducer creator', () => {
    const reducer = createReducer((state, action) => ({
          locations: [...state.locations, action.payload]
    })).matching(locationReceived);

    const newState = reducer(LocationsType)(emptyState, locationReceived('a location'));

    expect(newState).toEqual({
      locations: ['a location']
    })
  });

});