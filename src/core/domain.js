import t from 'tcomb';
import { FSA } from './types';


class Domain {
  constructor(name) {
    this.name = name;
    this.actions = [];
    this.reducers = [];
  }

  getName() {
    return this.name;
  }
}

export default Domain;