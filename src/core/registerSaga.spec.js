import t from 'tcomb';

import { takeEvery, call } from 'redux-saga/effects';

import registerSaga from './registerSaga';
import createAction from './createAction';

const dumbAction = createAction('dumbAction', t.String);
const actionToListen = createAction('actionToListen', t.String);

const aSaga = function* () {
  yield put(dumbAction('TRY'));
};

const registeredSaga = registerSaga(aSaga).matchingEvery(actionToListen);

describe('register saga', () => {
  test('returns a saga', () => {
    const next = registeredSaga().next();

    expect(next.value).toEqual(takeEvery('ACTION_TO_LISTEN', aSaga));
  })
});