import t from 'tcomb';

export default (type, initState, name = undefined) => {

  if(!t.isType(type)) {
    type = t.struct(type, name);
  }

  if(type.meta.name === undefined) {
    throw new TypeError('Tcomb struct used to build a state must have a name');
  }

  return {
    initialState: type(initState),
    meta: type
  }
};