import t from 'tcomb';
import _ from 'lodash';

export default (name, type = t.Nil) => thunk => {
    type = (!t.isType(type) && _.isObject(type)) ? t.struct(type) : type;

    const actionName = `${name}ActionCreator`;

    return t.func([t.maybe(type)], t.Function, actionName).of((payload = undefined) => thunk(payload));
}