import t from 'tcomb';

export const FSA = (payloadType, name = 'FSA') => t.interface(
  {
    type: t.String,
    payload: payloadType,
    error: t.Boolean,
  }, name);