import t from 'tcomb';

import registerStore from './registerStore';

const LocationsType = t.struct({
  locations: t.list(t.String)
}, 'Locations');

describe('initialize state', () => {
  test('returns type & initial state', () => {
    const state = registerStore(LocationsType, {
      locations: []
    });

    expect(state).toMatchObject({
      meta: LocationsType,
      initialState: {
        locations: []
      }
    });
  });

  test('builds type from a given descriptor', () => {
    const state = registerStore({
      locations: t.list(t.String)
    },{
      locations: []
    }, 'Locations');

    expect(state.meta.meta).toMatchObject({
      kind: 'struct',
      name: 'Locations'
    });
  });
});