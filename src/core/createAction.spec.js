import createAction from './createAction';
import t from 'tcomb';

describe('createAction', () => {

  test('returns a default FSA', () => {
    const action = createAction('locationReceived');

    expect(action()).toMatchObject({
      type: 'LOCATION_RECEIVED',
      payload: undefined,
      error: false
    });
  });

  test('returns a default value, if not provided', () => {
    const action = (args = 'a location') => createAction('locationReceived', t.String)(args);

    expect(action()).toMatchObject({
      type: 'LOCATION_RECEIVED',
      payload: 'a location',
      error: false
    });
  });

  test('returns a given payload', () => {
    const locationReceived = createAction('locationReceived', t.String);

    const action = locationReceived('a location');

    expect(action).toMatchObject({
      type: 'LOCATION_RECEIVED',
      payload: 'a location',
      error: false
    });
  });

  describe('with error', () => {

    test('returns a default fsa', () => {
      const locationReceived = createAction('locationReceived');

      const action = locationReceived(true);

      expect(action).toMatchObject({
        error: true
      });
    });

    test('returns a fsa with given value', () => {
      const locationReceived = createAction('locationReceived', t.String);

      const action = locationReceived('a location', true);

      expect(action).toMatchObject({
        error: true
      });
    });

    test('has a default value and returns a fsa with given value', () => {
      const locationReceived = (args = 'a location', error = false) => createAction('locationReceived', t.String)(args, error);

      const action = locationReceived('another location', true);

      expect(action).toMatchObject({
        payload: 'another location',
        error: true
      })
    });
  });
});
