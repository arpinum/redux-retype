import t from 'tcomb';
import createAction from './createAction';
import createReducer from './createReducer';
import registerStore from './registerStore';
import registerReducer from './registerReducer';

describe('register reducer', () => {
  test('returns the initialState during initialization', () => {
    const locationReceived = createAction('locationReceived', t.String);

    const locationReceivedReducer = createReducer((state, action) => ({
      locations: {$push: [action.payload]}
    })).matching(locationReceived);

    const locationStore =  registerStore({
      locations: t.list(t.String)
    }, {
      locations: []
    }, 'Locations');

    const reducer = registerReducer([locationReceivedReducer]).reducing(locationStore);
    const emptyState = reducer(undefined, { type: '@@redux/INIT' });

    expect(emptyState).toEqual({
      locations: []
    });
  });

  test('returns the new state', () => {
    const locationReceived = createAction('locationReceived', t.String);

    const locationReceivedReducer = createReducer((state, action) => ({
      locations: {$push: [action.payload]}
    })).matching(locationReceived);

    const locationStore =  registerStore({
      locations: t.list(t.String)
    }, {
      locations: []
    }, 'Locations');

    const reducer = registerReducer([locationReceivedReducer]).reducing(locationStore);
    const emptyState = reducer(undefined, { type: '@@redux/INIT' });

    const newState = reducer(emptyState, locationReceived('a new location'));

    expect(newState).toEqual({
      locations: ['a new location']
    });
  });
});